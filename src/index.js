import { tmpdir } from 'os';
import formidable from 'formidable';

const fileUpload = (options) => async (ctx, next) => {
    const defaultOptions = {
        maxFiles: 1,
        maxFields: 5,
        maxFieldsSize: 1024 * 1024,
        uploadDir: process.env.UPLOAD_DIR || tmpdir(),
        keepExtensions: true,
    };
    const form = formidable(options || defaultOptions);

    try {
        await new Promise((resolve, reject) => {
            form.parse(ctx.req, (err, fields, files) => {
                if (err) {
                    reject(err);
                    return;
                }
                ctx.request.files = files;
                ctx.request.fields = fields;
                resolve();
            });
        });
    } catch(err) {
        ctx.set('Content-Type', 'text/plain');
        ctx.status = err.httpCode;
        ctx.body = "Upload failed";
        return;
    }
    await next();
}

export default fileUpload;
