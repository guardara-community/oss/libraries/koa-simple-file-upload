# Simple File Upload Koa Middleware

This is a very simple file upload middleware for [Koa](https://koajs.com) that directly uses [Formidable](https://github.com/node-formidable/formidable) to upload files.

This middleware was not intended for public consumption. Accordingly, it is not published on npm and the project does not accept contributions and feature requests.
